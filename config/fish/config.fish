if status is-interactive
    # Commands to run in interactive sessions can go here
end


##################################
# General
##################################

# Fish Theme Stuff
# See https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md#bobthefish
set -g theme_powerline_fonts yes
set -g theme_color_scheme terminal2-dark
set -g theme_date_format "+%H:%M:%S"

set -gx EDITOR nvim
set -gx VISUAL nvim
